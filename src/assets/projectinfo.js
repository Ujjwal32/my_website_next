export const projects = [
    {
        id: 1,
        title: 'Trader\'s Domain',
        detail: 'Trader\'s domain is a financial news portal website that provides a user with latest market news and also shows the stock prices. One can read the articles, see the stock prices and  analyze the candlestick pattern using the chart displayed for the company he/she needs along with fundamental summary of the company.',
        thumbnail: 'https://github.com/Ujjwal32/Traders_Domain-MERN-/blob/master/screenshot/home.jpg?raw=true',
        github: 'https://github.com/Ujjwal32/Traders_Domain-MERN-',
        demo: 'https://mern-traders-domain.herokuapp.com/',
        tech_stack: ['node','react','mongodb'],
    },
    {
        id: 2,
        title: 'React Movie app',
        detail: 'A simple movie/series finder app built with reactjs and with the use of an external api. This enables user to search the movies they want and read synopsis as well as ratings and other informations.',
        thumbnail: 'https://github.com/Ujjwal32/React-MovieApp/blob/master/public/homepage.png?raw=true',
        github: 'https://github.com/Ujjwal32/React-MovieApp',
        demo: 'https://reactmovieclone.netlify.app/',
        tech_stack: ['react','css'],
    }
]