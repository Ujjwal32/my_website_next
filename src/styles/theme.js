import { extendTheme } from "@chakra-ui/react"
import { createBreakpoints } from "@chakra-ui/theme-tools"

const breakpoints = createBreakpoints({
    xs: '24em',
    sm: "30em",
    md: "48em",
    lg: "62em",
    xl: "80em",
    "2xl": "96em",
})
  
const theme = extendTheme({
    styles:{
        global:{
            a: {
                _hover: {
                    borderBottom: "2px solid #168BF8",
                },
                _active:{
                    borderBottom: "2px solid #168BF8",
                }
            }
        }
    },
    fontSizes: {
        xs: "0.75rem",
        sm: "0.875rem",
        md: "1rem",
        lg: "1.25em",
        xl: "1.5rem",
        "2xl": "1.75rem",
        "3xl": "1.875rem",
        "4xl": "2.25rem",
        "5xl": "3rem",
        "6xl": "3.75rem",
        "7xl": "4.5rem",
        "8xl": "6rem",
        "9xl": "8rem",
      },
    breakpoints
})


export default theme