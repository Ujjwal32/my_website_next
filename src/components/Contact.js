import { Box, Button, Flex, Heading, HStack, Input, Stack, VStack } from '@chakra-ui/react'
import React, { useState } from 'react'
import { BiEnvelope } from 'react-icons/bi'
import { useToast } from "@chakra-ui/react"

function Contact() {
    const [email,setEmail] = useState({
        name: 'the hell',
        from: '',
        message: ''
    })
    const toast = useToast()
    const changeHandler = (e) => {
        setEmail({...email,[e.target.name]: e.target.value})
    }
    const sendEmail = async () => {
        fetch('/api',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify(email)
            }).then(res => res.json()).then((data) => {
                console.log(data)
                toast({
                    title: `${data.msg}`,
                    description: "I will get to you soon",
                    status: "success",
                    duration: 2000,
                    isClosable: true,
                  })
                setEmail({
                name: '',
                from: '',
                message: ''
                })
            }).catch( err => {
                console.log(err)
                toast({
                    title: `${data.msg}`,
                    description: "Sorry, something went wrong.",
                    status: "failed",
                    duration: 2000,
                    isClosable: true,
                  })
            })
    }
    return (
        <Box id='contact' maxH={['auto','auto','auto','80vh']} margin={['20px 2.5% 0','40px 2.5% 0','40px 5% 0',]} overflow='hidden' width={['95%','95%','90%',]} bg='#1c1c1c' borderRadius={['','','lg']}>
            <Flex flexDirection={['column','column','column','row']} align='stretch'> 
            {/* width='90%' margin='20px 5%' */}
            <Box minW={['100%','100%','100%','50%']} bg='#131313' minH={['60vh','60vh','70vh','100vh']} overflow='hidden' padding={['20px','20px','0']}>
                <VStack spacing={12}>
                    <Heading as='h1' fontSize={['lg','lg','xl','3xl']} color='#168BF8' textAlign='center' margin={['20px 40px','20px 100px','20px 100px','20px 50px','20px 100px']}>I would love to hear from you</Heading>
                    <HStack spacing='-50px'>
                    <Box height={['200px','200px','300px']} width={['200px','200px','300px']} border='1px white solid' borderRadius='full' />
                    <Box height={['200px','200px','300px']} width={['200px','200px','300px']} border='1px white dotted' borderRadius='full'/>
                    <Box height={['200px','200px','300px']} width={['200px','200px','300px']} border='1px white solid' borderRadius='full' />
                    </HStack>
                </VStack>
            </Box>
            <Box minW='50%' padding='30px'>
            <Heading as='h1' size='xl' color='#FBBF24'>Contact Me</Heading>
            <Stack spacing={8} direction={'column'} marginTop={10}>
                <Input variant="flushed" placeholder="Name" name='name' defaultValue={email.name} onChange={changeHandler}/>
                <Input type='email' name='from' variant="flushed" placeholder="Email" defaultValue={email.from} onChange={changeHandler}/>
                <Input variant="flushed" name='message' placeholder="Message" defaultValue={email.message} onChange={changeHandler}/>
                <Button rightIcon={<BiEnvelope />} width='5em' variant='outline' type='submit' onClick={sendEmail}>Send</Button>
            </Stack>
            </Box>
            </Flex>
        </Box>
    )
}

export default Contact
