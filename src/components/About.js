import { Avatar, Box, Container, Heading, HStack, VStack,Link as ChakraLink, ButtonGroup, Button } from '@chakra-ui/react'
import React from 'react'
import { FaGithub, FaLinkedin, FaTwitter } from 'react-icons/fa'

function About() {
    return (
        <Box id='about' margin={['20px 2.5% 0','40px 2.5% 0','40px 5% 0',]} borderRadius={['','','lg']} width={['95%','95%','90%',]} padding='20px 0px' overflow='hidden' bg='#1c1c1c' >
            <VStack  spacing='20px' >
                <Heading as='h1' size='2xl' color='#FBBF24'>
                    About me
                </Heading>
                <Avatar name="Ujjwal Singh Basnet" size='2xl' src={"https://bit.ly/3iwblHC"} />
                <Heading as='h4' size='md' color='#168BF8'>
                    Ujjwal Singh Basnet
                </Heading>
                <Container minW='60vw' align='center'>
                    There are many benefits to a joint design and development system. Not only
                    does it bring benefits to the design team, but it also brings benefits to
                    engineering teams. It makes sure that our experiences have a consistent look
                    and feel, not just in our design specs, but in production
                </Container>
                <HStack fontSize='2em' spacing='30px'>
                    <ChakraLink href='https://www.github.com/Ujjwal32' isExternal><FaGithub /></ChakraLink>
                    <ChakraLink href='https://www.twitter.com/Ujjwal32' isExternal><FaTwitter /></ChakraLink>
                    <ChakraLink href='https://www.twitter.com/Ujjwal32' isExternal><FaLinkedin /></ChakraLink>
                </HStack>
            </VStack>
        </Box>
    )
}

export default About
