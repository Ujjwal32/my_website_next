import { Box, Heading, Spacer, Stack } from '@chakra-ui/react'
import React from 'react'
import Navigation from './Navigation'
import { FaReact } from 'react-icons/fa'
import Link from 'next/link'

function Hero() {

    return (
        <Box id='home' h='100vh' margin={['0 2.5%','0 2.5%','0 5%',]} padding={['0 10px','0 20px','0 40px',]} overflow='hidden' width={['95%','95%','90%',]} bg='#1c1c1c' borderRadius='lg'>
            <Navigation />
            <Stack mt={['20vh','20vh','20vh','30vh']} height='50vh' align='flex-start' direction='column'>
            <Heading as='h4' size='md' color='#168BF8'>
                {
                    `<Hello />`
                }
            </Heading>
            <Heading as='h1' size='2xl'>
                I am Ujjwal 
            </Heading>
            <Heading as='h1' fontSize={['xl','xl','2xl','4xl']} color='#168BF8'>
                A Fullstack Developer
            </Heading>
            <Spacer maxH={4}/>
            <a href="https://drive.google.com/file/d/10a3aFRkTU4BIl404m84KUgVkn8bGPdhf/view?usp=sharing" target="_blank" download><Box as='button' bg='#FBBF24' padding='5px 12px' color='#000000' borderRadius='10px'>Download CV</Box></a>
            
            </Stack>
            <Box marginTop={['-20vh','-20vh','-20vh','-30vh','-50vh']} marginLeft={['0','0','0','52vw']} fontSize={['100vw','100vw','80vw','40vw',]} marginBottom={['0','0','0','0']}>
                <FaReact  style={{transform: 'rotate(105deg)',opacity:'0.4',color:'#168BF8'}}/>
            </Box>
        </Box>
    )
}

export default Hero
