import { Box, Button, ButtonGroup, Container, Heading, VStack } from '@chakra-ui/react'
import React from 'react'
import { projects } from '../assets/projectinfo'
import SingleProject from './SingleProject'
function Projects() {
    return (
        <Box id = 'projects' minH={'100vh'} margin={['20px 2.5% 0','40px 2.5% 0','40px 5% 0',]} borderRadius={['','','lg']} width={['95%','95%','90%',]} padding={['20px 0px','20px 0px','40px 0px',]} overflow='hidden' bg='#1c1c1c' >
                <VStack spacing='20px'>
                    <Heading as='h1' size='2xl' color='#FBBF24'>
                        My Projects
                    </Heading>
                    <Container minW='60vw' align='center'>
                        There are many benefits to a joint design and development system. Not only
                        does it bring benefits to the design team, but it also brings benefits to
                        engineering teams. It makes sure that our experiences have a consistent look
                        and feel, not just in our design specs, but in production
                    </Container>
                    {/* <ButtonGroup variant="outline" spacing="6">
                        <Button colorScheme="yellow" variant='solid'>All</Button>
                        <Button colorScheme="teal" variant='solid'>React</Button>
                        <Button colorScheme="green" variant='solid'>Node</Button>
                        <Button colorScheme="blue" variant='solid'>Fullstack</Button>
                    </ButtonGroup> */}
                </VStack>
                <Box>
                    {
                        projects.map( project => <SingleProject project={project} key={project.id}/>)
                    }
                </Box>
        </Box>
    )
}

export default Projects
