import { Box, Flex, Heading, IconButton, Link as ChakraLink, Spacer} from '@chakra-ui/react'
import React, { useState } from 'react'
import Link from 'next/link'
import { GiHamburgerMenu } from 'react-icons/gi'
import { VscClose } from 'react-icons/vsc'
function Navigation() {
    const [showNav,setShownav] = useState(false)

    const openNav = () => {
        setShownav(true)
    }
    return (
            <>
                <Flex align='start' h={['8vh','8vh','8vh','10vh']} borderBottom='1px solid rgba(255, 255, 255,0.5)'>
                    <Box padding={2}>
                        <Heading as='h4' size='md' color='#FBBF24' fontSize={['md','md','xl','2xl']}>
                            {
                                `<Logo />`
                            }
                        </Heading>
                    </Box>
                    <Spacer maxW={10}/>
                    <Flex 
                        padding={2}
                        minW={'38vw'}
                        align='start' 
                        justify='space-between' 
                        h='5vh' 
                        display={['none','none','none','flex','flex']}>
                        <ChakraLink as={Link} href='#home'>Home</ChakraLink>
                        <ChakraLink as={Link} href='#about'>About</ChakraLink>
                        <ChakraLink as={Link} href='#skills'>Skills</ChakraLink>
                        <ChakraLink as={Link} href='#projects'>Projects</ChakraLink>
                        <ChakraLink as={Link} href='#contact'>Contact</ChakraLink>
                    </Flex>
                    <Spacer />
                    <IconButton display={['flex','flex','flex','none','none']} bg='none' fontSize='2em'>
                        <GiHamburgerMenu onClick={() => setShownav(true)}/>
                    </IconButton>
                    <Box display={['none','none','none','flex','flex']}>
                    <Box marginTop={2} as='button' bg='#FBBF24' h='30px' w='70px' color='#000000' borderRadius='10px'>Blog</Box>
                    </Box>
                </Flex>
                <Flex direction='column' width={['100vw','100vw','50vw']} height='100vh' zIndex='100' bg='#121212' align='stretch' position='absolute' top='0' right='0' padding='40px' textAlign='center' display={showNav ? 'flex' : 'none'}>
                    <Box>
                        <VscClose fontSize='2em' cursor='pointer' onClick={() => setShownav(false)}/>
                    </Box>
                    <ChakraLink as={Link} href='#home'>Home</ChakraLink>
                    <Spacer />
                    <ChakraLink as={Link} href='#about'>About</ChakraLink>
                    <Spacer />
                    <ChakraLink as={Link} href='#skills'>Skills</ChakraLink>
                    <Spacer />
                    <ChakraLink as={Link} href='#projects'>Projects</ChakraLink>
                    <Spacer />
                    <ChakraLink as={Link} href='#contact'>Contact</ChakraLink>
                    <Spacer />
                    <ChakraLink as={Link} href='#contact'>Blog</ChakraLink>
                </Flex>
            </>
    )
}

export default Navigation
