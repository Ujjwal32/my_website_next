import { Box } from '@chakra-ui/react'
import React from 'react'

function Footer() {
    return (
        <Box as='footer' marginTop='40px' paddingBottom='10px' textAlign='center'>
        <p> Ujjwal Singh Basnet . copyright &copy; {(new Date).getFullYear()}</p>
      </Box>
    )
}

export default Footer
