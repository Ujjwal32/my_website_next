import { Box } from '@chakra-ui/react'
import React, { useEffect, useState } from 'react'
import { AiOutlineArrowUp } from 'react-icons/ai'

function Backtotop() { 
    const [display,setDisplay] = useState('none')

    const backtotop = () => {
        window.scroll(0,0)
    }
    const handleScroll = () => {
        if(window.scrollY >= 100){
            setDisplay('grid')
        } else {
            setDisplay('none')
        }
    }
    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    });
    return (
        <Box style={style} display={display} className='arrow' onClick={backtotop}>
            <AiOutlineArrowUp />
        </Box>
    )
}

const style = {
    position: 'fixed',
    background: '#FBBF24',
    height:'2em',
    width: '2em',
    borderRadius: '50%',
    bottom: '5vh',
    right: '20px',
    placeItems: 'center',
    cursor: 'pointer'
}
export default Backtotop
