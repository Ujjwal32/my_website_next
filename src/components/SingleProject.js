import { Box, Button, ButtonGroup, Container,Image, Flex, Stack } from '@chakra-ui/react'
import React from 'react'
import { FaGithub, FaNodeJs, FaReact } from 'react-icons/fa'
import { DiMongodb } from "react-icons/di";
import { BiLinkExternal } from "react-icons/bi";
const obj = {
    node: <FaNodeJs color='#75b859'/>,
    react: <FaReact color='#61dafb'/>,
    mongodb: <DiMongodb color='#13aa52'/>
}
function SingleProject({project}) {
    return (
        <Flex flexDirection={['column','column','column',`${project.id % 2 === 0 ? 'row-reverse': 'row'}`]} width={['95%','95%','90%',]} margin={['10px 2.5%','20px 2.5%','20px 5%']} minH={['','','40vh']} align='stretch'>
            <Box width={['100%','100%','100%','50%']} marginBottom='0px' minHeight={['','','40vh']}>
                <Image src={project.thumbnail} alt={`${project.title} snapshot`} fit='cover' height='100%'/>
            </Box>
            <Box bg='#121212'>
            <Stack direction='column' padding=' 20px' spacing={[2,3,4]}>
                <ButtonGroup fontSize='2em'>
                    {
                        project.tech_stack.map((tech,index) => <div key={project.id + index}>{obj[tech]}</div>)
                    }
                </ButtonGroup>
                <Container minWidth='100%' padding='0'>
                    {project.detail}
                </Container>
                <ButtonGroup>
                    <Button rightIcon={<BiLinkExternal />} bg='#168BF8'>View demo</Button>
                    <Button rightIcon={<FaGithub />} bgColor='#FBBF24'>View Code</Button>
                </ButtonGroup>
            </Stack>
            </Box>
        </Flex>
    )
}

export default SingleProject
