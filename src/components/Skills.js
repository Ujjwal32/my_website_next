import { Box, Heading, HStack, VStack } from '@chakra-ui/react'
import React from 'react'
import { FaCss3Alt, FaHtml5, FaNodeJs, FaReact } from 'react-icons/fa'
import { SiMongodb } from 'react-icons/si'
import { DiJavascript1 } from 'react-icons/di'

function Skills() {
    return (
        <Box id = 'skills' minH={'20vh'} margin={['20px 2.5% 0','40px 2.5% 0','40px 5% 0',]} borderRadius={['','','lg']} width={['95%','95%','90%',]} padding={['20px 0px','20px 0px','40px 0px',]} overflow='hidden' bg='#1c1c1c' >
            <VStack  spacing='40px' >
                <Heading as='h1' size='2xl' color='#FBBF24'>
                    Skills
                </Heading>
                <HStack fontSize={['3xl','4xl','5xl','7xl']} spacing={8} opacity='0.6'>
                    <FaReact color='#168bf8'/>
                    <FaNodeJs color='#168bf8'/>
                    <DiJavascript1 color='#168bf8'/>
                    <FaHtml5 color='#168bf8'/>
                    <FaCss3Alt color='#168bf8'/>
                    <SiMongodb color='#168bf8'/>
                </HStack>
            </VStack>
        </Box>
    )
}

export default Skills
