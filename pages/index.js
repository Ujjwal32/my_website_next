import Head from 'next/head'
import { Box } from '@chakra-ui/react'
import Hero from '../src/components/Hero'
import About from '../src/components/About'
import Projects from '../src/components/Projects'
import Contact from '../src/components/Contact'
import Footer from '../src/components/Footer'
import Skills from '../src/components/Skills'
import Backtotop from '../src/components/Backtotop'
export default function Home() {
  const scrollTrigger = (e) => {
    console.log(e.height || 'scroll')
  }
  return (
    <>
    <div onScroll={scrollTrigger}>
      <Head>
        <title>Home | Ujjwal Singh Basnet</title>
        <meta name="developer portfolio" content="fullstack developer portfolio nodejs developer backend" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <Box bg='#131313' color='white' >
        <Backtotop />
        <Hero />
        <About />
        <Skills />
        <Projects />
        <Contact />
        <Footer />
      </Box>
    </div>
    </>
  )
}
