import nodemailer from 'nodemailer'
import { google } from 'googleapis'

const htmlEmail = `
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .container{
        width:50vw; 
        height: 300px; 
        background:#4feed0;
        margin:0 auto;
        padding:10px;
        }
        .header{
            margin-top: 30px;
        }
        img{
            height: 100px;
            width: 100px
        }
        p{
            text-align: center;
            margin-top: 30px;
        }
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class='container'>
    <tr>
        <td align="center">
            <h4 class='header'>Hello, how are you?</h4>
        </td>
    </tr>
    <tr>
        <td align="center">
        <img src='https://res.cloudinary.com/dm3x42mup/image/upload/v1623664734/buy-it/users/rklpvwk5q2xu79pd8srz.png' />
        </td>
    </tr>
    <tr>
        <td align="center">
            <p>I am testing nodemailer to send emails from my application.</p>
        </td>
    </tr>
</table>
</body>
</html>
`
const clientid = '1031323557368-37h47bdrfksn2vhpbdrrnsf0o7klf4aa.apps.googleusercontent.com'
const clientsecret = 'CkYTu92vhC6vXjtKW1doRYiv'
const redireacturi = 'https://developers.google.com/oauthplayground'
const refreshtoken = '1//04xYkj-HWwidJCgYIARAAGAQSNwF-L9IriTcpPgFalJDNUm8OZcmvq35EgHovB81xqAVntcZQ7TyIVls57DaOpaSnH2Gf1qDzaIU'

const oauth2Client = new google.auth.OAuth2(clientid,clientsecret,redireacturi)
oauth2Client.setCredentials({ refresh_token: refreshtoken})

async function main(body) {
    const { name, from, message } = body
    console.log(from)
    const htmlEmail = `
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
            .container{
            width:50vw; 
            height: 300px; 
            background:#4feed0;
            margin:0 auto;
            padding:10px;
            }
            .header{
                margin-top: 30px;
            }
            img{
                height: 100px;
                width: 100px
            }
            p{
                text-align: center;
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class='container'>
        <tr>
            <td align="center">
                <h4 class='header'>From: ${from} ${name}</h4>
            </td>
        </tr>
        <tr>
            <td align="center">
            <img src='https://res.cloudinary.com/dm3x42mup/image/upload/v1623664734/buy-it/users/rklpvwk5q2xu79pd8srz.png' />
            </td>
        </tr>
        <tr>
            <td align="center">
                <p>${message}</p>
            </td>
        </tr>
    </table>
    </body>
    </html>
`
    const accessToken = await oauth2Client.getAccessToken()  
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      service: "gmail",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        type: 'OAuth2',
        user: 'ujjwalbas167@gmail.com', // generated ethereal user
        clientId: clientid, // generated ethereal password
        clientSecret: clientsecret,
        refreshToken: refreshtoken,
        accessToken: accessToken
      },
    });
  
    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: `${name} <${from}>`,
      to: "ujjwalbas167@gmail.com", 
      subject: "From Portfolio", 
      text: `From: ${from} ${message}`,
      html: htmlEmail, // html body
    });
}
  
export default function handler(req, res) {
    main(req.body).catch( err => {
        console.log(err)
        res.json({
            msg: 'Failed'
        })
    })
    res.json({msg: 'Message sent successfully!'})
  }